﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace tp1
{
    class Program
    {
        //Fonction qui retourne une string qui, 
        //à partir du nom et prénom de l'utilisateur,
        //renvoi une seule chaîne de caractère correctement formatée comme suit: "prénom NOM"
        public static string FormatPrenomNom(string prenom, string nom)
        {
            string nomFormate = prenom.ToLower() + " " + nom.ToUpper();
            return nomFormate;
        }

        //Fonction de type float qui retourne l'IMC de l'utilisateur à partir de sa taille et de son poids
        public static float IMC(int taille, float poids)
        {
            float tailleF = (float)taille;
            float imc = poids / ((tailleF / 100) * (tailleF / 100));

            return imc;
        }

        //Fonction qui commente l'IMC de l'utilisateur en fonction de minitère de la santé 
        public static void IMC2(int taille, float poids)
        {
            float valIMC = IMC(taille, poids);

            const string stade1 = "Attention à l'anorexie";
            const string stade2 = "Vous êtes un peu maigrichon";
            const string stade3 = "Vous êtes de corpulence normale";
            const string stade4 = "Vous êtes en surpoids";
            const string stade5 = "Obésité modérée !";
            const string stade6 = "Obésité sévère !";
            const string stade7 = "Obésité morbide !";

            if (valIMC < 16.5)
            {
                Console.WriteLine(stade1);
            }
            else if (valIMC >= 16.5 && valIMC < 18.5)
            {
                Console.WriteLine(stade2);
            }
            else if (valIMC >= 18.5 && valIMC < 25)
            {
                Console.WriteLine(stade3);
            }
            else if (valIMC >= 25 && valIMC < 30)
            {
                Console.WriteLine(stade4);
            }
            else if (valIMC >= 30 && valIMC < 35)
            {
                Console.WriteLine(stade5);
            }
            else if (valIMC >= 35 && valIMC < 40)
            {
                Console.WriteLine(stade6);
            }
            else
            {
                Console.WriteLine(stade7);
            }
        }

        //Fonction qui demande à l'utilisateur d'estimer le nombre de cheveux qu'il a sur la tête
        public static void devineNombreCheveux()
        {
            bool found = false;

            while (!found)
            {
                int nombreUtilisateur = -1;
                if (int.TryParse(Console.ReadLine(), out nombreUtilisateur))
                {
                    if (nombreUtilisateur > 100000 && nombreUtilisateur < 150000)
                    {
                        found = true;
                        Console.WriteLine("Trop fort");
                    }
                    else
                    {
                        Console.WriteLine("Essaye encore");
                    }
                }
                else
                {
                    Console.WriteLine("Veuillez entrer un nombre");
                }
            }
        }

        public static void Main(string[] args)
        {

            bool recommProg = true;

            while (recommProg)
            {
                recommProg = false;
                Console.WriteLine("Bienvenu sur mon programme, jeune étranger imberbe.");

                //Affichage du nom et du prénom de l'utilisateur
                Console.WriteLine("Quel est ton nom ?");
                string nom = Console.ReadLine();
                Console.WriteLine("Quel est ton prénom ?");
                string prenom = Console.ReadLine();

                Console.WriteLine("salut " + prenom + " " + nom + " !");


                //Affichage de la taille, du poids et de l'âge de l'utilisateur
                Console.WriteLine("Quel est ta taille (en cm) ?");
                int taille = int.Parse(Console.ReadLine());
                Console.WriteLine("Quel est ton poids (en kg) ?");
                float poids = float.Parse(Console.ReadLine());
                Console.WriteLine("Quel est ton âge ?");
                int age = int.Parse(Console.ReadLine());

                Console.WriteLine("Tu mesures " + taille + " cm.");
                Console.WriteLine("Ton poids est de " + poids + " kg.");
                Console.WriteLine("Tu as " + age + " ans.");


                //Vérification de l'âge de l'utilisateur
                if (age < 18)
                {
                    Console.WriteLine("Tu n'as pas le droit d'acheter de l'alcool.");
                }
                else
                {
                    Console.WriteLine("C'est cool.");
                }


                //Nom et prénom de l'utilisateur sous le format "prénom NOM"
                Console.WriteLine("Voici ton prénom et ton nom au format: prénom NOM \n" + FormatPrenomNom(prenom, nom));


                //Affichage de l'IMC de l'utilisateur
                Console.WriteLine("Voici ton IMC en fonction de ta taille et de ton poids \n" + IMC(taille, poids).ToString());

                //Affichage de commentaire selon l'IMC de l'utilisateur
                IMC2(taille, poids);

                //Demande à l'utilisateur d'estimer le nombre de cheveux qu'il a sur la tête
                Console.WriteLine("Combien penses-tu avoir de cheveux sur la tête ?");
                devineNombreCheveux();

                //Sécurisation du programme
                //Regarde si le nom contient un chiffre
                if (nom.Any(char.IsDigit))
                {
                    Console.WriteLine("Le nom contient au moins un chiffre");
                }
                //Regarde si le prenom contient un chiffre
                else if (prenom.Any(char.IsDigit))
                {
                    Console.WriteLine("Le prenom contient au moins un chiffre");
                }
                //Regarde si la taille est supérieure à 0
                else if (taille > 0)
                {
                    Console.WriteLine("La taille doit être supérieur à 0");
                }
                //Regarde si le poids est supérieur à 0
                else if (poids > 0)
                {
                    Console.WriteLine("Le poids doit être supérieur à 0");
                }
                //Regarde si l'âge est supérieur à 0
                else if (prenom.Any(char.IsDigit))
                {
                    Console.WriteLine("Je pense qu'il faut être né pour avoir un âge");
                }
                else
                {
                    Console.WriteLine("Tes conditions sont ok");
                }
            }

                //Laissons choisir l'utilisateur..
                Console.WriteLine("Voici 4 possibilités : choisi 1) pour quitter le programme, " +
                                  "2) pour recommencer le programme, " +
                                  "3) pour compter jusqu'à 10" +
                                  "et 4) pour téléphoner à Tata Jacqueline.");
                int choix = int.Parse(Console.ReadLine());

                //Quitter le programme
                if (choix == 1)
                {
                    Console.WriteLine("Au revoir");
                    Thread.Sleep(3000);
                    return;
                }
                //Recommencer le programme
                else if (choix == 2)
                {
                    recommProg = true;
                }
                //Compter jusqu'à 10
                else if (choix == 3)
                {
                    for (int i = 1; i <= 10; i++)
                    {
                        Console.WriteLine(i);
                        Thread.Sleep(1000);
                    }
                    return;
                }
                //Téléphoner à Tata Jacqueline
                else
                {
                    Console.WriteLine("Désolée mais il est tard je suis plus trop inventive, bonne nuit Tata Jacqueline.");
                    Thread.Sleep(3000);
                    return;
                }


                Thread.Sleep(3000);
        }
    }
}


