﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvadersArmory
{
    public class ArmeImporteur
    {
        public static void importFromFile(string filename)
        {
            string[] lines = System.IO.File.ReadAllLines(filename);
            foreach(string line in lines)
            {
                string[] words = line.Split(' ');
                EWeaponType type;
                switch (words[1])
                {
                    case "Direct":
                        type = EWeaponType.Direct;
                        break;
                    case "Explosif":
                        type = EWeaponType.Explosive;
                        break;
                    case "Guided":
                        type = EWeaponType.Guided;
                        break;
                    default:
                        type = EWeaponType.Direct;
                        break;
                }
                Armory.CreatBlueprint(words[0], type, Double.Parse(words[2]), Double.Parse(words[3]));
            }
        }
    }
}
