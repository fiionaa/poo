﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Models;
using Models.SpaceShips;
using System.Windows.Controls.Primitives;
using SpaceInvadersArmory;

namespace TP4
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Player> Players { get; private set; } = new List<Player>();
        public List<Spaceship> Spaceships { get; private set; } = new List<Spaceship>();

        private Player selectedPlayer;

        public MainWindow()
        {
            InitializeComponent();
            refreshListPlayer();
        }

        private void Shopping_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                ArmeImporteur.importFromFile(openFileDialog.FileName);
        }

        private void Apparence_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                selectedPlayer.BattleShip.PicturePath = openFileDialog.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(openFileDialog.FileName);
                bitmap.EndInit();
                ((Image)this.FindName("SpaceShipPicture")).Source = bitmap;

                ((Label)this.FindName("ImageError")).Visibility = Visibility.Hidden;
            }
                
        }

        private void add_Player_Click(object sender, RoutedEventArgs e)
        {
            Window_add_player formPlayer = new Window_add_player();
            Button validation = (Button)formPlayer.FindName("ValidateButton");
            validation.Click += new RoutedEventHandler(add_Player);
            formPlayer.Show();
        }

        private void remove_player_Click(object sender, RoutedEventArgs e)
        {
            ListView listJoueurs = (ListView)this.FindName("ListViewJoueur");
            if(listJoueurs.SelectedItem != null)
            {
                Player p = (Player)listJoueurs.SelectedItems[0];
                Players.Remove(p);
                refreshListPlayer();
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un joueur");
            }
        }
        private void Init()
        {
            Players = new List<Player>();
            Spaceships = new List<Spaceship>();
            Players.Add(new Player("MaXiMe", "haRlé", "Per6fleur"));
            Players.Add(new Player("Guillaume", "urban", "LaGrandeRoutière"));
            Players.Add(new Player("kintali asish-kumar", "PRusty", "DxC"));
            Spaceships.Add(Players[0].BattleShip);
            Spaceships.Add(new Dart(false));
            Spaceships.Add(new B_Wings(false));
            Spaceships.Add(new Rocinante(false));
            Spaceships.Add(new F_18(false));
            Spaceships.Add(new Tardis(false));
            Random rng = new Random();
            //on mélange la liste
            Spaceships.Sort((x, y) => rng.Next(-1, 2));
        }
        public void playGame()
        {
            Armory.ViewArmory();
            Statistiques.incrementNbParties();
            foreach (var item in Players)
            {
                Console.WriteLine(item.ToString());
                item.BattleShip.ViewShip();
            }
            while (Spaceships.Where(x => !x.BelongsPlayer && !x.IsDestroyed).ToList().Count > 0 && !Players[0].BattleShip.IsDestroyed)
            {
                PlayRound();
            }
            if (!Players[0].BattleShip.IsDestroyed)
            {
                Console.WriteLine("Gagné !");
                Statistiques.incrementNbPartiesGagnees();
            }
            else
            {
                Console.WriteLine("Perdu !");
                Statistiques.incrementNbPartiesPerdu();
            }
            Console.ReadKey();
        }
        public void PlayRound()
        {
            Console.WriteLine("");
            Console.WriteLine("=========== Tour ===========");
            Random rng = new Random();
            bool playerHasShoot = false;
            int nbrIt = 1;
            List<Spaceship> tmp = Spaceships.Where(x => !x.IsDestroyed).ToList();
            foreach (var item in tmp)
            {
                IAbility ship = item as IAbility;
                if (ship != null) { ship.UseAbility(Spaceships); }
                item.ReloadWeapons();
            }
            foreach (var item in tmp)
            {
                if (!playerHasShoot)
                {
                    tmp = Spaceships.Where(x => !x.BelongsPlayer && !x.IsDestroyed).ToList();
                    double chancesForPlayer = ((double)nbrIt / tmp.Count) * 100.0;
                    if (rng.Next(0, 101) < chancesForPlayer)
                    {
                        playerHasShoot = true;
                        Spaceship target = tmp[rng.Next(0, tmp.Count)];
                        Players[0].BattleShip.ShootTarget(target);
                    }
                }
                if (!item.BelongsPlayer && !item.IsDestroyed)
                {
                    item.ShootTarget(Players[0].BattleShip);
                }
                nbrIt++;
            }
            foreach (var item in Spaceships)
            {
                if (!item.IsDestroyed) { item.RepairShield(2); }
            }
        }
        void add_Player(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            Window_add_player windowAddPlayer = (Window_add_player)((Grid)b.Parent).Parent;
            String firstName = ((TextBox)windowAddPlayer.FindName("inputFirstName")).Text;
            String lastName = ((TextBox)windowAddPlayer.FindName("inputLastName")).Text;
            String alias = ((TextBox)windowAddPlayer.FindName("inputAlias")).Text;
            if(String.IsNullOrEmpty(alias) || String.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
            {
                MessageBox.Show("Veuillez remplir tous les champs");
            }
            else
            {
                Players.Add(new Player(firstName, lastName, alias));
                windowAddPlayer.Hide();
                refreshListPlayer();
            }

        }
        void refreshListPlayer()
        {
            ListView listJoueurs = (ListView)this.FindName("ListViewJoueur");
            listJoueurs.Items.Clear();
            foreach (Player p in Players)
            {
                listJoueurs.Items.Add(p);
            }
        }
        private void onChangeListViewSelect(object sender, SelectionChangedEventArgs e)
        {
            if(((ListView)sender).SelectedItem != null)
            {
                Button appearanceButton = (Button)this.FindName("AppearanceButton");
                appearanceButton.Visibility = Visibility.Visible;
                Player p = (Player)((ListView)sender).SelectedItem;
                selectedPlayer = p;
                Spaceship vaisseau = p.BattleShip;
                ((Label)this.FindName("PlayerName")).Content = p.Alias;
                ((Label)this.FindName("NomVaisseau")).Content = vaisseau.Name;
                ((Label)this.FindName("StructVaisseau")).Content = vaisseau.Structure.ToString();
                ((Label)this.FindName("BouclierVaisseau")).Content = vaisseau.Shield.ToString();
                ((Label)this.FindName("DegatsVaisseaux")).Content = vaisseau.AverageDamages.ToString();
                if (!String.IsNullOrEmpty(vaisseau.PicturePath))
                {
                    ((Image)this.FindName("SpaceShipPicture")).Source = new BitmapImage(new Uri(vaisseau.PicturePath));
                }
                else
                {
                    ((Label)this.FindName("ImageError")).Visibility = Visibility.Visible;
                    ((Image)this.FindName("SpaceShipPicture")).Visibility = Visibility.Hidden;
                }
                Button showPlayerButton = (Button)this.FindName("ShowPlayerButton");
                showPlayerButton.Visibility = Visibility.Visible;
                showPlayerButton.Click += new RoutedEventHandler(this.showPlayer);

                Button updatePlayerButton = (Button)this.FindName("UpdatePlayerButton");
                updatePlayerButton.Visibility = Visibility.Visible;
                updatePlayerButton.Click += new RoutedEventHandler(this.updatePlayer);

                refreshListWeapons(vaisseau);
                ((Button)this.FindName("AddWeaponButton")).Visibility = Visibility.Visible;
                ((Button)this.FindName("RemoveWeaponButton")).Visibility = Visibility.Visible;
            }
        }

        private void updatePlayer(object sender, RoutedEventArgs e)
        {
            updatePlayer windowUpdate = new updatePlayer();
            windowUpdate.Show();
            ((Button)windowUpdate.FindName("validateButton")).Click += new RoutedEventHandler(this.updatePlayerAction);
        }

        private void updatePlayerAction(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            updatePlayer windowUpdate = (updatePlayer)((Grid)b.Parent).Parent;
            TextBox firstNameInput = (TextBox)windowUpdate.FindName("firstNameInput");
            if (!String.IsNullOrEmpty(firstNameInput.Text))
            {
                selectedPlayer.FirstName = firstNameInput.Text;
            }
            TextBox lastNameInput = (TextBox)windowUpdate.FindName("lastNameInput");
            if (!String.IsNullOrEmpty(lastNameInput.Text))
            {
                selectedPlayer.LastName = lastNameInput.Text;
            }
            TextBox aliasInput = (TextBox)windowUpdate.FindName("aliasInput");
            if (!String.IsNullOrEmpty(aliasInput.Text))
            {
                selectedPlayer.Alias = aliasInput.Text;
            }
            windowUpdate.Hide();
            refreshListPlayer();
        }

        private void showPlayer(object sender, RoutedEventArgs e)
        {
            showPlayer windowShowPlayer = new showPlayer();
            ((Label)windowShowPlayer.FindName("firstNameLabel")).Content = selectedPlayer.FirstName;
            ((Label)windowShowPlayer.FindName("lastNameLabel")).Content = selectedPlayer.LastName;
            ((Label)windowShowPlayer.FindName("aliasLabel")).Content = selectedPlayer.Alias;
            windowShowPlayer.Show();
        }

        private void refreshListWeapons(Spaceship vaisseau)
        {
            ListView listArmes = (ListView)this.FindName("listArmes");
            listArmes.Items.Clear();
            foreach (Weapon w in vaisseau.Weapons)
            {
                listArmes.Items.Add(w);
            }
        }

        private void addWeapon_Click(object sender, RoutedEventArgs e)
        {
            addWeapon windowAddWeapon = new addWeapon();
            ListView listArmes = (ListView)windowAddWeapon.FindName("ListArmes");
            foreach(WeaponBlueprint w in Armory.getAllWeapons())
            {
                listArmes.Items.Add(w);
            }
            windowAddWeapon.Show();
            Button validateButton = (Button)windowAddWeapon.FindName("validate");
            validateButton.Click += new RoutedEventHandler(this.addWeapon_Action);
        }

        private void addWeapon_Action(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            addWeapon window = (addWeapon)((Grid)b.Parent).Parent;
            ListView armes = (ListView)window.FindName("ListArmes");
            if(armes.SelectedItem != null)
            {
                Weapon w = Armory.CreatWeapon((WeaponBlueprint)armes.SelectedItem);
                try
                {
                    selectedPlayer.BattleShip.AddWeapon(w);
                }
                catch(Exception exception)
                {
                    MessageBox.Show("Nombre d'armes max atteint");
                }
                window.Hide();
                refreshListWeapons(selectedPlayer.BattleShip);
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner une arme");
            }
        }

        private void RemoveWeaponButton_Click(object sender, RoutedEventArgs e)
        {
            ListView armes = (ListView)this.FindName("listArmes");
            if(listArmes.SelectedItem != null)
            {
                selectedPlayer.BattleShip.RemoveWeapon((Weapon)armes.SelectedItem);
                refreshListWeapons(selectedPlayer.BattleShip);
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner une arme");
            }
        }

        private void Stats_Click(object sender, RoutedEventArgs e)
        {
            StatsWindow statsWindow = new StatsWindow();
            statsWindow.Show();
        }
    }
}
