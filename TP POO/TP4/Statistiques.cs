﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    class Statistiques
    {
        public static int nbParties = 0;
        public static int nbPartiesGagne = 0;
        public static int nbPartiesPerdu = 0;
        public static void incrementNbParties()
        {
            nbParties++;
        }
        public static void incrementNbPartiesPerdu()
        {
            nbPartiesPerdu++;
        }
        public static void incrementNbPartiesGagnees()
        {
            nbPartiesGagne++;
        }
    }
}
