﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp2
{
    public class Dart : Vaisseau
    {
        public Dart()
        {
            this.m_ptStructureMax = 10;
            this.valeurActuelleStructure = 10;
            this.valeurActuelleBouclier = 3;
            this.m_ptBouclierMax = 3;

            this.m_armes = new List<Arme>();

            this.m_armes.Add(new Arme("Laser", 2, 3, 0, 1));
        }

        public override void Attaque(Vaisseau v)
        {
            foreach(Arme a in m_armes){
                if (a.GetType() == 0)
                {
                    a.SetCompteur(1);
                    v.repartirPointsDegats(a.SimulerTir());
                }
            }
        }
    }
}