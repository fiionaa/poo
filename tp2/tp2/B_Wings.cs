﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp2
{
    class B_Wings : Vaisseau
    {
        public B_Wings()
        {
            this.m_ptStructureMax = 30;
            this.valeurActuelleStructure = 30;
            this.valeurActuelleBouclier = 0;
            this.m_ptBouclierMax = 0;

            this.m_armes = new List<Arme>();

            this.m_armes.Add(new Arme("Hammer", 1, 8, 1, 1.5));
        }

        public override void Attaque(Vaisseau v)
        {
            foreach (Arme a in m_armes)
            {
                if ((int)a.GetType() == 1)
                {
                    a.SetCompteur(1);
                    v.repartirPointsDegats(a.SimulerTir());
                }
            }
        }
    }
}
