﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp2
{
    public class Joueur
    {

        private string m_nom;
        private string m_prenom;
        private string m_pseudo;

        public Joueur(string nom, string prenom, string pseudo)
        {
            this.m_nom = FormaterNomPrenom(nom);
            this.m_prenom = FormaterNomPrenom(prenom);
            this.m_pseudo = pseudo;
        }

        public string GetNom()
        {
            return m_nom;
        }

        public string GetPrenom()
        {
            return m_prenom;
        }

        public string GetPseudo()
        {
            return m_pseudo;
        }

        public void SetPseudo(string pseudo)
        {
            m_pseudo = pseudo;
        }

        private static string FormaterNomPrenom(string nom)
        {
            return nom[0].ToString().ToUpper() + nom.Substring(1, nom.Length - 1).ToLower();
        }

        public override string ToString()
        {
            return "Le joueur créé est " + m_pseudo + "(" + m_prenom + "," + m_nom + ")";
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Joueur j = obj as Joueur;
                if (m_pseudo == j.m_pseudo)
                {
                    return false;
                }
                return true;
            }
        }

        public override int GetHashCode()
        {
            return m_prenom.GetHashCode() ^ m_nom.GetHashCode() ^ m_pseudo.GetHashCode();
        }
    }
}
