﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp2
{
    public class ArmurerieException : Exception
    {

        public ArmurerieException()
            : base(String.Format("L'arme ne fait pas partie d'une armurerie"))
        {
        }
    }
}
