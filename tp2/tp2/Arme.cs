﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp2
{
    public class Arme
    {
        private string m_nomArme;
        private int m_degatMin;
        private int m_degatMax;
        private Type m_typeArme;
        private double m_temps_rechargement;
        private double m_compteur;

        public enum Type
        {
            Direct,
            Explosif,
            Guidé
        }

        public Arme(string nomArme, int degatMin, int degatMax, int type, double temps_rechargement)
        {
            this.m_nomArme = nomArme;
            this.m_degatMin = degatMin;
            this.m_degatMax = degatMax;
            this.m_typeArme = (Type)type;
            this.m_temps_rechargement = temps_rechargement;
            this.m_compteur = temps_rechargement;
        }

        public string GetNomArme()
        {
            return m_nomArme;
        }

        public int GetDegatMin()
        {
            return m_degatMin;
        }

        public int GetDegatMax()
        {
            return m_degatMax;
        }


        public void SetNomArme(string nomArme)
        {
            m_nomArme = nomArme;
        }

        public void SetDegatMin(int degatMin)
        {
            m_degatMin = degatMin;
        }

        public void SetDegatMax(int degatMax)
        {
            m_degatMax = degatMax;
        }

        public Type GetType()
        {
            return m_typeArme;
        }

        public double GetCompteur()
        {
            return m_compteur;
        }

        public void SetCompteur(double compteur)
        {
            m_compteur = compteur;
        }

        public int SimulerTir()
        {
            m_compteur -= 1;
            Random alea = new Random();
            Random dir = new Random();
            int nbDegats = 0;

            if (m_compteur == 0) //degat = 0
            {
                if (m_typeArme == (Type)0)
                {
                    if (dir.Next(11) == 1)
                    {
                        nbDegats = 0;
                    }
                    else
                    {
                        nbDegats = alea.Next(this.GetDegatMin(), this.GetDegatMax());
                    }
                }
                else if (m_typeArme == (Type)1)
                {
                    if (dir.Next(5) == 1)
                    {
                        nbDegats = 0;
                    }
                    else
                    {
                        m_temps_rechargement = m_temps_rechargement * 2;
                        nbDegats = (alea.Next(this.GetDegatMin(), this.GetDegatMax()))*2;
                    }
                }
                else
                {
                    nbDegats = this.GetDegatMin();
                }
            }
            return nbDegats;
        }
    }
}