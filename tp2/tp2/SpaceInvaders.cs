﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace tp2
{
    public class SpaceInvaders
    {
        private static List<Vaisseau> vaisseaux = new List<Vaisseau>();
        private static Vaisseau monVaisseau;

        public SpaceInvaders()
        {
        }

        private static void Tour()
        {  
            foreach(Vaisseau v in vaisseaux)
            {
                //Rajout des points de bouclier
                if (v.GetPtBouclierMax() != v.getValeurActuelleStructure())
                {
                    if(v.GetPtBouclierMax() - v.getValeurActuelleStructure() > 2)
                    {
                        v.setValeurActuelleBouclier((int)v.getValeurActuelleBouclier() + 2);
                    }
                    else
                    {
                        v.setValeurActuelleBouclier((int)v.GetPtBouclierMax());
                    }
                }
                //Les vaisseaux utilisent leurs aptitudes
                if (v is IAptitude)
                {
                    IAptitude v2 = (IAptitude)v;
                    v2.Utiliser(vaisseaux);
                }
            }

            int positionJoueur = 1;
            bool posititionJoueurTrouvee = false;
            while (!posititionJoueurTrouvee)
            {
                for(int j =0; j < positionJoueur; j++)
                {
                    Random random = new Random();
                    int randomInt = random.Next(0, vaisseaux.Count);
                    if(randomInt == 1)
                    {
                        posititionJoueurTrouvee = true;
                    }
                }
                positionJoueur += 1;
            }

            positionJoueur -= 1;
            int i = 0;
            while(i < vaisseaux.Count)
            {
                if(i == positionJoueur - 1)
                {
                    //Le joueur attaque un vaisseau random
                    Random vais = new Random();
                    int index = vais.Next(0, vaisseaux.Count);
                    Vaisseau vaisseauRandom = vaisseaux[index];
                    if (vaisseauRandom.getValeurActuelleStructure() != 0)
                    {
                        monVaisseau.Attaque(vaisseauRandom);
                    }
                }
                else
                {
                    Vaisseau ennemi = vaisseaux[i];
                    ennemi.Attaque(monVaisseau);
                    i++;
                }
            }
        }

        private static void Init()
        {
            Joueur j1 = new Joueur("Facchini", "Fiona", "fifi");
            Console.WriteLine(j1.ToString());
            Joueur j2 = new Joueur("Facchini", "Claude", "cfac");
            Console.WriteLine(j2.ToString());
            Joueur j3 = new Joueur("Facchini", "Ashley", "h");
            Console.WriteLine(j3.ToString());

            monVaisseau = new Tardis();

            vaisseaux.Add(new B_Wings());
            vaisseaux.Add(new Dart());
            vaisseaux.Add(new F_18());
            vaisseaux.Add(new Rocinante());
            vaisseaux.Add(new Tardis());
            vaisseaux.Add(new ViperMKII());
        }

        static void Main(string[] args)
        {

            SpaceInvaders.Init();

            List<Arme> armes = new List<Arme>();
            armes.Add(new Arme("Arme 1", 10, 40, 0,1));
            armes.Add(new Arme("Arme 2", 20, 50, 1,1));
            armes.Add(new Arme("Arme 3", 30, 70, 2,1));

            Armurerie armurie = new Armurerie();

            Joueur j1 = new Joueur("Facchini", "Fiona", "Fifi");

            armurie.toString();

            bool vaisseauEnnemiVivant = true;

            while(monVaisseau.getValeurActuelleStructure() > 0 && vaisseauEnnemiVivant)
            {
                Tour();
                vaisseauEnnemiVivant = false;
                foreach(Vaisseau v in vaisseaux)
                {
                    if(v.getValeurActuelleStructure() > 0)
                    {
                        vaisseauEnnemiVivant = true;
                    }
                }
            }
        }
    }
}
